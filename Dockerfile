FROM alpine:3.7 as downloader
ARG caddy_version="0.10.14"
LABEL caddy_version="${caddy_version}"
WORKDIR "/tmp"
RUN apk add --no-cache wget tar gzip
RUN wget https://github.com/mholt/caddy/releases/download/v${caddy_version}/caddy_v${caddy_version}_linux_amd64.tar.gz
RUN tar -xzf caddy*.tar.gz caddy

# Final image
FROM alpine:3.7
LABEL maintainer "@woernfl <florian.woerner@onmyown.io>"
RUN apk add --no-cache libcap
COPY Caddyfile /etc/Caddyfile
COPY --from=downloader /tmp/caddy /bin/caddy
RUN setcap CAP_NET_BIND_SERVICE=+eip /bin/caddy
USER nobody
EXPOSE 80 443 2015
ENTRYPOINT ["/bin/caddy"]
CMD ["--conf", "/etc/Caddyfile", "--log", "stdout", "--agree=true"]